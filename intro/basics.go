package main

// Imports can be used with parentheses to avoid repeating the work "import"
import (
	"fmt"
	"math"
	"math/cmplx"
)

/*
	1. Parameter types come after the names
	2. If two or more parameters share a type, omit the type definition
*/
func sum(x, y int) int {
	return x + y
}

/*
	3. Functions can return any number of results
*/
func swap(x, y string) (string, string) {
	return y, x
}

/*
	4. Functions can name each of the return values, they'll be treated as values declared on the top of the function
*/
func divide(first, second int) (quotient, remainder int) {
	quotient = first / second
	remainder = first % second
	return // This line is an example of a "naked return"
}

/*
	5. Variables can be declared in the package level
	6. If there's a value to initialize (initializer), the type can be omitted
*/
var month, year int // Without initializer
var currentYear, nextYear = 2019, 2020

/*
	7. Constants are declared like variables but using the keyword "const"
*/
const ten = 10

func RunBasics() {
	// Using a member exported from "fmt"
	fmt.Printf("Greetings, universe!\n")

	// Only members with names which start with capital letters are exported in Go (Pi is exported, pi isn't)
	fmt.Printf("Pi value:\t%.2f\n", math.Pi)

	// Using a user-defined function
	fmt.Println(sum(3, 2))

	// Using a function which returns more than one value. The type of the variables "a" and "b" is inferred from the function header
	// The := is a shortcut to avoid using the "var" keyword
	a, b := swap("Greetings", "universe")

	fmt.Printf("First: %s\t | Second: %s\n", a, b)

	// Using a function with named return
	x, y := divide(10, 3)

	fmt.Printf("Quotient: %d\t | Remainder: %d\n", x, y)

	// To declare a variable defining its type, put it after the variable list name
	var integer int

	fmt.Printf("Local: %d\t | Global: %d\n", integer, year)
	fmt.Printf("Current year: %d\t | Next year: %d\n", currentYear, nextYear)

	var imaginary complex128 = cmplx.Sqrt(-5 + 12i)
	fmt.Printf("Imaginary type: %T\t | Imaginary value: %v\n", imaginary, imaginary)

	// Converting between types (cast) is done using an expression: T(v). T is the type and v is the value
	floatValue := 5.5
	integerValue := int(floatValue)
	// Go requires the explicit cast (to make sure you know what's going on) even for types which are "larger"
	//anotherFloatValue := integerValue // This line uncommented issues a warning
	fmt.Printf("Float value: %f\t | Integer value: %d\n", floatValue, integerValue)

	// Printing a global constant
	fmt.Printf("Global constant: %d\n", ten)
}
