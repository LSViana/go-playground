package main

import (
	"fmt"
	"runtime"
	"time"
)

func f() int {
	return 1
}

func deferFunction() {
	fmt.Println("Deferred function call")
}

func deferSurrounding() {
	defer deferFunction()

	// Call multiple defers inside the function creates a stack, which the last function deferred will be the first executed
	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	fmt.Println("Defer surrounding function call")
}

func RunFlowControl() {
	// The basic for to perform the sum of all numbers between 0 and 10
	/*
		1. The curly brackets are always necessary
	*/
	sum := 0
	for i := 0; i < 10; i++ {
		sum += i
	}
	fmt.Printf("Sum: %d\n", sum)

	// Just like other programming languages, Go can omit the init and post parts of a for structure
	/*
		2. Omitting the init and post parts of a for structure will automatically strip the semicolons and give us a "while" (which is still named "for")
	*/
	sum = 1
	for sum < 1000 {
		sum += sum
	}
	fmt.Printf("Sum: %d\n", sum)

	/*
		3. Omitting all parts of a for structure creates an infinite loop
	*/
	if false {
		for {

		}
	}

	/*
		4. if statements also don't need parentheses, but the usage of curly brackets is mandatory
	*/
	if sum == 1024 {
		fmt.Println("We got the result!")
	}

	/*
		5. If statements may contain an initialization statement, variables declared there are in scope only inside the if
	*/
	if v := sum - 24; v == 1000 {
		fmt.Printf("Variable v is from if's initializer: %d\n", v)
	}

	/*
		6. Go's switch structure doesn't need breaks (the language only runs the selected case)
		7. The values in cases need not be constants
	*/
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X")
	case "linux":
		fmt.Println("Linux")
	default:
		fmt.Printf("%s\n", os)
	}

	/*
		8. The values of the cases in a switch structure are evaluated from top to bottom
	*/
	// In the following switch, the function f() will never be executed
	switch i := 0; i {
	case 0:
		fmt.Println("Always executes this")
	case f():
		fmt.Println("Never executes this")
	}

	/*
		9. Switch statements omitting the condition is considered as switch true { ... }
	*/
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon!")
	default:
		fmt.Println("Good evening!")
	}

	/*
		10. The defer keyword defers the execution of that line until the surrounding function finishes its execution
	*/
	deferSurrounding()
}
